vim-gtk-doc-comments :: possible tasks
======================================

Have a common base implementation between Vim and Emacs
-------------------------------------------------------

The initial Vim plugin was a port of the Emacs plugin, implemented exclusively
in the Vim scripting language. Instead of re-implementing this for each text
editor, have an external program that reads stdin and writes the result to
stdout, and then it can be wired up easily in each text editor.

This is the approach taken by gcu-lineup-parameters that is part of
[gdev-c-utils](https://gitlab.gnome.org/swilmet/gdev-c-utils).
