vim-gtk-doc-comments
====================

Based on [gnome-doc.vim](https://www.vim.org/scripts/script.php?script_id=306)
and this
[gnome-doc.vim git repository](https://github.com/vim-scripts/gnome-doc.vim).

Add a document header to your function. The headers can be parsed and converted
into documentation by the [gtk-doc](https://gitlab.gnome.org/GNOME/gtk-doc)
tools.

https://gitlab.gnome.org/swilmet/vim-gtk-doc-comments

See also
--------

- [vim-swilmet-config](https://gitlab.gnome.org/swilmet/vim-swilmet-config)
